package com.example.home.stopwatch;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.btn_start)
    Button btnStart;
    @BindView(R.id.count)
    EditText count;
    @BindView(R.id.timer)
    TextView timer;

    private Handler handler;
    private int counter;
    private boolean isRunning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                timer.setText(String.valueOf(counter));
                if (isRunning) {
                    counter--;
                    if (counter == 0) {
                        Toast.makeText(getApplicationContext(), "that's all", Toast.LENGTH_SHORT).show();
                        startTimer();
                    }
                }
                handler.postDelayed(this, 1000);
            }
        });

    }

    @OnTextChanged(R.id.count)
    public void setCounter(Editable editable) {
        counter = Integer.valueOf(String.valueOf(editable));
    }

    @OnClick(R.id.btn_start)
    public void startTimer() {
        isRunning = !isRunning;
        count.setEnabled(!isRunning);
        if (isRunning) {
            btnStart.setText("pause");
        } else {
            btnStart.setText("start");
        }
    }
}
